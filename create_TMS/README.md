# Create TMS in PNG and WEBP format

## Create TMS from QGIS
We used the tool "Generating XYZ tiles (MBTiles)" from QGIS to generate the PNG files.

## Convert PNG to WEBP
On windows use the script and library in this folder to convert all PNG files to WEBP files.
