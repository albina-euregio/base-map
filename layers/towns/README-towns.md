# Orte
- albina_orte_p: places (to be extendet only central europe)
- capital_cities: capital cities 
- towns_zeichen: Cities whose labels are displayed in Cyrillic characters and in the English translation
- verbauung_a: big cities (whole europe) polygon layer

## albina_orte_p erweitern
- Shape-Files gis_osm_places_free_1 (Geofabrik)
- Attribut fclass. city, town (choose manualy)
- style, category, level_x select via size of locations (by selecting the size of the locations based on size and appearance attribute "Population");
 l_ext_o, l_inn_o indicate the position of the label depending on the symbol; TRE_levelx: 0 (only for Trentino 1)
