# Strassen
- hauptverkehrswege_l: Major roads, highways, trunk roads (to be extended)
- residential: residantial roads of austria
- roads: small raods in italy
- secondary_roads: secondary roads in austria 
- tertiary_roads: tertiary roads in austria 

the last three are curently in the work-in-progress folder because they are not in the project that is curently online (if needed or lost they can be found in the commit 21.03.2023)

## hauptverkehrswege_l to extend:
- Shape-Files: gis_osm_roads_free_1 (Geofabrik)
- Attribut fclass: motorway + manual selection from trunk and primary
- Attribute: style:2311 (Motorway) 2321 (other big roads), level_6,7,8: 0, level_9:depending on the size, level_10=1
