# Base map
This QGIS Project contains a worldwide base map that is used in the communication of avalanche danger. The focus of the development is on a discreet map that nevertheless allows for quick orientation. For this purpose, the background was kept in grey and decisive map contents such as mountain peaks or mountain groups were added.

## Projection
All layers use EPSG:4326 - WGS84

## Digital Elevation Models (DEMs)
DEMs were not added to the GIT project and can be downloaded from the following URLs:

- https://sonny.4lima.de/ (xxxx by Sonny.tif)
- https://data.europa.eu/data/datasets/eu-dem?locale=en (DEPRECATED) (EUD_CP-DEMS_xx000xx000-AA.tif)

Both sources are under the Creative Commons Attribution 4.0 license. More Information to the licenses can be found in the folder licenses.

## Shape-Files
- https://www.openstreetmap.org
- https://download.geofabrik.de/
- http://overpass-turbo.eu/
- https://www.naturalearthdata.com/downloads/110m-cultural-vectors/ (ne_110m_admin_0_countries)
- https://www.naturalearthdata.com/downloads/10m-physical-vectors/ (ne_10m_land)
- https://hub.arcgis.com/datasets/esri::world-cities/explore?location=39.556470%2C10.368615%2C5.26 (World Cities)

## Structure
- clipping_masks: masks to clip DEMs along country borders
- create_TMS: files and help for creating TMS in png and webp format
- DEM: empty folder to put DEMs into
- work_in_progress: empty folder to include experimental layers and data
- helpful_files: folder with helpful files to work with this map
- layers
	- boundaries
	- cablecars
	- label
	- peaks
	- roads
	- towns
	- waters
	- land

## Test
The base map can be seen on https://simon04.github.io/eaws-bulletin-map/
